﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {
	private Transform trans;
	private GameObject body;

	void Start() {
		trans = transform;
		body = gameObject;
	}


	void Update() {
			trans.Translate(0, 0, 1f);
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name.Contains("Bullet") || col.gameObject.name.Contains("destroy")){
			if (PoolManagerScript.Instance.isActive()) {
				body.SetActive(false);
				}
		}
		if(col.gameObject.name.Contains("Ship")){
			Destroy(col.gameObject);
		}
	}
}
