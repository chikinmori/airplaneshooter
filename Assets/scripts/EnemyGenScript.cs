﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Random.Range (-10.0f, 10.0f) > 9.7f) {
			if (PoolManagerScript.Instance.isActive ()) {
				GameObject bullet = PoolManagerScript.Instance.getEnemy ();
				if (bullet != null) {
					Transform bulletTrans = bullet.transform;
					bulletTrans.position = new Vector3 (Random.Range (-25f, 25f), Random.Range (-25f, 25f), 165f);
					bullet.SetActive (true);
				}
			}
		}
	}
}
