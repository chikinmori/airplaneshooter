﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManagerScript : MonoBehaviour {
	public GameObject bulletPrefab;
	public GameObject enemyPrefab;
	public int enemySize;
	public int bulletSize;
	public bool active;

	private List<List<GameObject>> genPool;
	private List<GameObject> bulletPool;
	private List<GameObject> enemyPool;

	private static PoolManagerScript _instance;

	public static PoolManagerScript Instance {
		get {
			if (_instance == null)
				_instance = (PoolManagerScript)FindObjectOfType(typeof(PoolManagerScript));
			return _instance;
		}
	}

	public bool isActive() {
		return active;
	}

	void Start() {
		if (isActive()) {
			genPool = new List<List<GameObject>>();
			bulletPool = new List<GameObject>();
			for (int i = 0; i < bulletSize; i++) {
				GameObject obj = (GameObject)Instantiate(bulletPrefab);
				obj.SetActive(false);
				bulletPool.Add(obj);
			}
			enemyPool = new List<GameObject>();
			for (int i = 0; i < enemySize; i++) {
				GameObject obj = (GameObject)Instantiate(enemyPrefab);
				obj.SetActive(false);
				enemyPool.Add(obj);
			}
		}
	}

	public GameObject getBullet() {
		for (int i = 0; i < bulletPool.Count; i++) {
			if (!bulletPool[i].activeInHierarchy) {
				return bulletPool[i];
			}
		}
		return null;
	}

	public GameObject getEnemy() {
		for (int i = 0; i < enemyPool.Count; i++) {
			if (!enemyPool[i].activeInHierarchy) {
				return enemyPool[i];
			}
		}
		return null;
	}

	public GameObject getEnemyPrefab() {
		return enemyPrefab;
	}

	public GameObject getBulletPrefab() {
		return bulletPrefab;
	}
}
