﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipScript : MonoBehaviour {
	private Transform trans;
	public float shipFireCoolDown;
	public int hp;
	private float coolDown;

	// Use this for initialization
	void Start () {
		trans = transform;
		coolDown = 0;
	}
	
	// Update is called once per frame
	void Update () {
		float hor = Input.GetAxis ("Horizontal");
		float ver = Input.GetAxis ("Vertical");
		float fir = Input.GetAxis ("Jump");

		if(hor != 0)
			trans.Translate(hor/2, 0, 0);
		if(ver != 0)
			trans.Translate(0, ver/2, 0);

		if (coolDown > 0)
			coolDown--;

		if (fir > 0) {
			if (coolDown <= 0) {
				if (PoolManagerScript.Instance.isActive()){
					GameObject bullet = PoolManagerScript.Instance.getBullet();
					if (bullet != null) {
						Transform bulletTrans = bullet.transform;
						bulletTrans.position = trans.position + new Vector3(0.7f, 1f, 0);
						bullet.SetActive(true);
					}//end if
				}else{
					Instantiate(PoolManagerScript.Instance.getBulletPrefab(), trans.position + new Vector3(0, 1f, 0),
						Quaternion.AngleAxis(0, Vector3.forward));
				}
				coolDown = shipFireCoolDown;
			}//end if
		}//end if
		if (hp <= 0)
			Destroy(gameObject);
			
	}
}
