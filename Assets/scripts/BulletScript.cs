﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	private Transform trans;
	private GameObject body;

	void Start() {
		trans = transform;
		body = gameObject;
	}


	void Update() {
		trans.Translate(0, -1f, 0);
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name.Contains("void")){
			if (PoolManagerScript.Instance.isActive()) {
				body.SetActive(false);
			}
		}
	}
}
